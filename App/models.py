from django.db import models

# Create your models here.


class Category(models.Model):

    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Company(models.Model):

    name = models.CharField(max_length=20)

    logo = models.ImageField()

    def __str__(self):
        return self.name


SCHOOL_TYPE = [
    ('praktijkonderwijs', 'praktijkonderwijs'),
    ('vmbo', 'vmbo'),
    ('mbo', 'mbo'),
    ('hbo', 'hbo'),
    ('opleidingsbedrijf', 'opleidingsbedrijf'),
]


class Product(models.Model):
    category = models.ForeignKey(
        Category, related_name="category", on_delete=models.CASCADE)
    company = models.ForeignKey(
        Company, related_name="company", on_delete=models.CASCADE)

    schooltype = models.CharField(
        max_length=25, choices=SCHOOL_TYPE)

    name = models.CharField(max_length=40)
    price = models.FloatField()
    photo = models.ImageField()

    description = models.TextField()
    publishing_end_date = models.DateTimeField()

from rest_framework import routers
from django.urls import include, path
from django.conf.urls import url
from .views import*
router = routers.DefaultRouter()

router.register(r'', ProductViewSet)


urlpatterns = [

    url('', include(router.urls)),


]

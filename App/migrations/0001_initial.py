# Generated by Django 3.1.2 on 2020-11-04 06:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('logo', models.ImageField(upload_to='')),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('schooltype', models.CharField(choices=[('praktijkonderwijs', 'praktijkonderwijs'), ('vmbo', 'vmbo'), ('mbo', 'mbo'), ('hbo', 'hbo'), ('opleidingsbedrijf', 'opleidingsbedrijf')], max_length=25)),
                ('name', models.CharField(max_length=40)),
                ('price', models.FloatField()),
                ('photo', models.ImageField(upload_to='')),
                ('description', models.TextField()),
                ('publishing_end_date', models.DateField()),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='category', to='App.category')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='company', to='App.company')),
            ],
        ),
    ]

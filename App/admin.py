from django.contrib import admin

from App.models import *
# Register your models here.


class AAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


admin.site.register(Category, AAdmin)

admin.site.register(Company, AAdmin)

admin.site.register(Product, AAdmin)

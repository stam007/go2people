from rest_framework import serializers
from App.models import *
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password',
                  'first_name', 'last_name', 'date_joined')
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):

        user = User.objects.create_user(**validated_data)

        return user


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'


class CompanySerializer(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False)
    company = CompanySerializer(many=False)

    class Meta:
        model = Product
        fields = '__all__'
